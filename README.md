Is your child's birthday party coming up? Have you already tried a clown and a magician? Well, how about a dog trick show?

Party Pooches supplies family-friendly dog trick entertainment for children's birthday parties and other events.

Dog trick demonstration, frisbee stunts, special "birthday child" interaction with the dogs & a fun photoshoot for all party goers (BYO camera).

Visit https://partypooches.com.au

Follow Party Pooches on Social Media
Instagram - https://instagram.com/officialpartypooches
Facebook - https://facebook.com/PartyPooches

Website: https://www.partypooches.com.au/